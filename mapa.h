#pragma once
#include <string>
#include <vector>


class Mapa {
    public:
        Mapa()=default;
        Mapa(const std::string &filename);
        Mapa(const Mapa &map);
        Mapa(const Mapa &&map);
        int prim(std::vector<int> & padres, const std::vector<int> *penalidades=nullptr) const;
        std::vector<std::vector<int>> data;
        int getSize() const;

        void computarProbabilidades();
        void computarCandidatos(int TAMVEC);

        std::vector<std::vector<double>> probabilidadInicial; // 1/distancia
        std::vector<std::vector<int>> candidatosDeNodo;

};