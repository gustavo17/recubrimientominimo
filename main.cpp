#include <opencv2/opencv.hpp>
#include <cstdio>
#include <iostream>
#include <sstream>
#include "mapa.h"

void execDot(const std::string& dotInput) {
    const char* cmd = "dot -Tpng -ooutput.png";
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "w"), pclose);

    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }

    // Escribir el contenido .dot en la entrada estándar de dot
    fwrite(dotInput.c_str(), sizeof(char), dotInput.size(), pipe.get());
}


int main (){
    Mapa m {"../berlin52.dat"};
    int n=m.data.size();

    std::vector <int> padres(n,-1);
    std::vector <std::vector<int>> arbol (n);
    const int cantidadDeCable=m.prim(padres);


    for(int i=1;i<n;i++){
        if (padres[i]!=-1) arbol[padres[i]].push_back(i);
    }

    std::stringstream ss;

    ss<<"digraph Arbol {\n";
    for (int i=1;i<n;i++){
        for (auto j:arbol[i]){
            ss<<i<<" -> "<<j<<" [label=\""<< m.data[i][j]<<"\"]\n";
        }
    }
    ss<<"}";
    execDot(ss.str());


     // Cargar la imagen PNG desde un archivo
    cv::Mat image = cv::imread("output.png", cv::IMREAD_COLOR);

    // Verificar si la imagen se ha cargado correctamente
    if (image.empty()) {
        std::cerr << "Error: No se pudo cargar la imagen." << std::endl;
        return 1;
    }

    // Mostrar la imagen en una ventana
    cv::namedWindow("Arbol Minimo", cv::WINDOW_AUTOSIZE);  // Crear una ventana con un nombre
    cv::imshow("Arbol Minimo", image);  // Mostrar la imagen en la ventana

    // Esperar a que el usuario presione una tecla
    cv::waitKey(0);

    // Cerrar la ventana y liberar los recursos
    cv::destroyAllWindows();
}