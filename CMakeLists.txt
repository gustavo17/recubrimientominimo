cmake_minimum_required(VERSION 3.26.0)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(gatsProject VERSION 0.1.0 LANGUAGES C CXX)

find_package(OpenCV REQUIRED)
add_executable(gatsProject main.cpp mapa.cpp)
target_link_libraries(gatsProject ${OpenCV_LIBS})
include_directories(${OpenCV_INCLUDE_DIRS})
